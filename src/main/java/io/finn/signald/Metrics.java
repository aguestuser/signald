package io.finn.signald;

import io.prometheus.client.Counter;
import io.prometheus.client.Histogram;

public class Metrics {

    // SINGLETON CONSTRUCTORS
    private Metrics() {}
    private static Metrics instance = new Metrics();
    public static Metrics getInstance() {
        return instance;
    }

    // WRITE METRICS

    final Histogram sendMessageLatencyTotal = Histogram
            .build()
            .name("send_message_latency_total")
            .help("Time required to send a signal message (in sec)")
            .register();

    final Histogram sendMessageLatencyFromNetworkCall = Histogram
            .build()
            .name("send_message_latency_network_call")
            .help("Time required to issue network call when sending message (in sec)")
            .register();

    final Histogram sendMessageLatencyFromContactRetrieval = Histogram
            .build()
            .name("send_message_latency_contact_retrieval")
            .help("Time required to retrieve contact when sending message (in sec)")
            .register();

    final Histogram sendMessageLatencyFromMessageConstruction = Histogram
            .build()
            .name("send_message_latency_message_construction")
            .help("Time required to retrieve contact when sending message (in sec)")
            .register();

    final Histogram sendMessageLatencyFromAccountUpdate = Histogram
            .build()
            .name("send_message_latency_account_update")
            .help("Time required to update account store when sending message (in sec)")
            .register();

    // READ METRICS

    final Counter readMessageCounter = Counter
            .build()
            .name("read_message_calls")
            .help("Number of times signald reads a message from the signal message pipe.")
            .register();

    final Counter readMessageTimeoutCounter = Counter
            .build()
            .name("read_message_timeouts")
            .help("Number of times signald times out when reading a message from the signal message pipe.")
            .register();

    final Histogram readMessageLatencyTotal = Histogram
            .build()
            .name("read_message_latency_total")
            .help("Time required to read an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencyCreateCache = Histogram
            .build()
            .name("read_message_latency_create_cache")
            .help("Time required to create message cache when reading an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencyDeleteCache = Histogram
            .build()
            .name("read_message_latency_delete_cache")
            .help("Time required to delete message cache when reading an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencyDecrypt = Histogram
            .build()
            .name("read_message_latency_decrypt")
            .help("Time required to decrypt message when reading an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencySocketBroadcast = Histogram
            .build()
            .name("read_message_latency_socket_broadcast")
            .help("Time required to broadcast to sockets when reading an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencyProcessMetadata = Histogram
            .build()
            .name("read_message_latency_process_metadata")
            .help("Time required to process all metadata when reading an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencyAccountUpdate = Histogram
            .build()
            .name("read_message_latency_process_metadata_account_update")
            .help("Time required to update account data when reading an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencyContactUpdate = Histogram
            .build()
            .name("read_message_latency_process_metadata_contact_update")
            .help("Time required to update contact store when reading an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencyResolveAddress = Histogram
            .build()
            .name("read_message_latency_process_metadata_resolve_address")
            .help("Time required to resolve an address when reading an incoming signal message (in sec)")
            .register();

    final Histogram readMessageLatencyProcessMetadataExpiryTime = Histogram
            .build()
            .name("read_message_latency_process_metadata_expiry_time")
            .help("Time required to process expiry time metadat when reading an incoming signal message (in sec)")
            .register();

    final Histogram getReadMessageLatencyProcessMetadataProfileKey = Histogram
            .build()
            .name("read_message_latency_process_metadata_profile_key")
            .help("Time required to process profile key metadata when reading an incoming signal message (in sec)")
            .register();

    final Histogram getReadMessageLatencyProcessMetadataAttachment = Histogram
            .build()
            .name("read_message_latency_process_metadata_attachment")
            .help("Time required to process attachment metadata when reading an incoming signal message (in sec)")
            .register();

}
